FROM centos:7

RUN yum update -y && yum install -y python3 python3-pip
RUN pip3 install flask flask_restful flask-jsonpify

COPY python-api.py /opt/python_api/python-api.py

ENTRYPOINT ["python3", "/opt/python_api/python-api.py"]
